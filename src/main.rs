#[macro_use]
extern crate text_io;

use std::env;

// Diagrama de Linus Pauling {{{
struct LinusStep {
    N: u8,
    L: u8,
} // camada, subcamada

const LINUS: [LinusStep; 19] = [
    LinusStep { N: 1, L: 0 },
    LinusStep { N: 2, L: 0 },
    LinusStep { N: 2, L: 1 },
    LinusStep { N: 3, L: 0 },
    LinusStep { N: 3, L: 1 },
    LinusStep { N: 4, L: 0 },
    LinusStep { N: 3, L: 2 },
    LinusStep { N: 4, L: 1 },
    LinusStep { N: 5, L: 0 },
    LinusStep { N: 4, L: 2 },
    LinusStep { N: 5, L: 1 },
    LinusStep { N: 6, L: 0 },
    LinusStep { N: 4, L: 3 },
    LinusStep { N: 5, L: 2 },
    LinusStep { N: 6, L: 1 },
    LinusStep { N: 7, L: 0 },
    LinusStep { N: 5, L: 3 },
    LinusStep { N: 6, L: 2 },
    LinusStep { N: 7, L: 1 },
]; // }}}

fn main() {

    if env::args().nth(1).is_some() {
	distriblinus_tex(env::args().nth(1).unwrap().parse::<u8>().unwrap()); // :(
	return;
    }

    println!("Escolha a função:");
    println!("    1) Elemento -> Distribuição");
    println!("    2) No de Elétrons -> Distribuição");
    println!("    3) Números Quânticos -> Elemento");
    println!("    4) Distribuição TeX");

    match read!() {
        1 => println!("UM!"),
        2 => noeletrons(),
        3 => println!("TRES"),
        4 => noeletrons_tex(),
        _ => println!("Opção não reconhecida, tente novamente."),
    }
}

fn noeletrons() {
    println!("Escreva o número de elétrons a serem distribuídos:");
    let n: u8 = read!();
    distriblinus(n);
}

fn noeletrons_tex() {
    println!("Escreva o número de elétrons a serem distribuídos:");
    let n: u8 = read!();
    distriblinus_tex(n);
}

fn distriblinus(mut no: u8) {
    // {{{
    for step in LINUS.iter() {
        let capacidade_subcamada = match step.L {
            0 => 2,
            1 => 6,
            2 => 10,
            3 => 14,
            _ => panic!(),
        };

        if no > capacidade_subcamada {
            no -= capacidade_subcamada;
            print!(
                "{}{} ",
                step.N,
                match step.L {
                    0 => "s²",
                    1 => "p⁶",
                    2 => "d¹⁰",
                    3 => "f¹⁴",
                    _ => panic!(),
                }
            );
        } else {
            println!(
                "{}{}{}",
                step.N,
                match step.L {
                    0 => "s",
                    1 => "p",
                    2 => "d",
                    3 => "f",
                    _ => panic!(),
                },
                superscript(no)
            );
            niveis(step.N, step.L, no as i8);
            numeros_quanticos(step.N, step.L, no as i8);
            break;
        }
    }
} // }}}

fn distriblinus_tex(mut no: u8) {
    // {{{
    for step in LINUS.iter() {
        let capacidade_subcamada = match step.L {
            0 => 2,
            1 => 6,
            2 => 10,
            3 => 14,
            _ => panic!(),
        };

        if no > capacidade_subcamada {
            no -= capacidade_subcamada;
            print!(
                "{}{} ",
                step.N,
                match step.L {
                    0 => "s$^{2}$",
                    1 => "p$^{6}$",
                    2 => "d$^{10}$",
                    3 => "f$^{14}$",
                    _ => panic!(),
                }
            );
        } else {
            println!(
                "{}{}$^{{{}}}$",
                step.N,
                match step.L {
                    0 => "s",
                    1 => "p",
                    2 => "d",
                    3 => "f",
                    _ => panic!(),
                },
                no //superscript(no)
            );
	    print!("{{\\par\\small ");
            niveis_tex(step.N, step.L, no as i8);
	    print!("}}");
            //numeros_quanticos(step.N, step.L, no as i8);
            break;
        }
    }
} // }}}

fn superscript(i: u8) -> &'static str {
    // {{{
    match i {
        1 => "¹",
        2 => "²",
        3 => "³",
        4 => "⁴",
        5 => "⁵",
        6 => "⁶",
        7 => "⁷",
        8 => "⁸",
        9 => "⁹",
        10 => "¹⁰",
        11 => "¹¹",
        12 => "¹²",
        13 => "¹³",
        14 => "¹⁴",
        _ => panic!(),
    }
} // }}}

fn numeros_quanticos(N: u8, L: u8, i: i8) {
    // {{{

    let mut ms = "*";
    let mut m: i8 = -10;
    if i > ((L as i8) * 2 + 1) {
        ms = "+";
        let j = i - ((L as i8) * 2 + 1);
        m = j - 1 - (L as i8);
    } else {
        ms = "-";
        m = i - 1 - (L as i8);
    }

    println!("");
    println!("");
    println!("Números Quânticos :");
    println!("    Principal (N) : {}", N);
    println!("    Secundário (L): {}", L);
    println!("    Magnético (m) : {}", m);
    println!("    Spin (ms)     : {}½", ms);
} // }}}

fn niveis(N: u8, L: u8, i: i8) {
    // {{{
    let mut camadas: [i8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
    for l in LINUS.iter() {
        if l.N == N && l.L == L {
            camadas[N as usize] += i;
            print!("K = {}", camadas[1]);
            if camadas[2] != 0 {
                print!("   L = {}", camadas[2]);
            }
            if camadas[3] != 0 {
                print!("   M = {}", camadas[3]);
            }
            if camadas[4] != 0 {
                print!("   N = {}", camadas[4]);
            }
            if camadas[5] != 0 {
                print!("   O = {}", camadas[5]);
            }
            if camadas[6] != 0 {
                print!("   P = {}", camadas[6]);
            }
            if camadas[7] != 0 {
                print!("   Q = {}", camadas[7]);
            }
            break;
        }
        camadas[l.N as usize] += match l.L {
            0 => 2,
            1 => 6,
            2 => 10,
            3 => 14,
            _ => panic!(),
        };
    }
} // }}}

fn niveis_tex(N: u8, L: u8, i: i8) {
    // {{{
    let mut camadas: [i8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
    for l in LINUS.iter() {
        if l.N == N && l.L == L {
            camadas[N as usize] += i;
            print!("K~=~{}", camadas[1]);
            if camadas[2] != 0 {
                print!(" ~~L~=~{}", camadas[2]);
            }
            if camadas[3] != 0 {
                print!(" ~~M~=~{}", camadas[3]);
            }
            if camadas[4] != 0 {
                print!(" ~~N~=~{}", camadas[4]);
            }
            if camadas[5] != 0 {
                print!(" ~~O~=~{}", camadas[5]);
            }
            if camadas[6] != 0 {
                print!(" ~~P~=~{}", camadas[6]);
            }
            if camadas[7] != 0 {
                print!(" ~~Q~=~{}", camadas[7]);
            }
            break;
        }
        camadas[l.N as usize] += match l.L {
            0 => 2,
            1 => 6,
            2 => 10,
            3 => 14,
            _ => panic!(),
        };
    }
} // }}}
